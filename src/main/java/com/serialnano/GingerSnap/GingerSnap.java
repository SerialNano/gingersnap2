/*
 * GingerSnap.java
 * v1.3.0
 * Last updated 05/02/2014
 * All Rights Reserved, J. Thibeault
 */

// What was changed, why was it changed, how was it changed

package com.serialnano.GingerSnap;

import org.bukkit.plugin.java.JavaPlugin;

import com.serialnano.GingerSnap.handlers.Teleport.TeleportObject;

public final class GingerSnap extends JavaPlugin {
	public String version = "1.3.0";
	public static TeleportObject[] teleports;
	
	@Override
	public void onEnable() {
		
		//Save the default config.yml if one doesnt exist
		this.saveDefaultConfig();

		this.getCommand("ignite").setExecutor(new CommandHandler());
		this.getCommand("ignite").setExecutor(new CommandHandler());
		this.getCommand("die").setExecutor(new CommandHandler());
		this.getCommand("ping").setExecutor(new CommandHandler());
		this.getCommand("height").setExecutor(new CommandHandler());
		this.getCommand("soar").setExecutor(new CommandHandler());
		this.getCommand("gingersnap").setExecutor(new CommandHandler());
		this.getCommand("fix").setExecutor(new CommandHandler());
		this.getCommand("bench").setExecutor(new CommandHandler());
		this.getCommand("announce").setExecutor(new CommandHandler());
		this.getCommand("boom").setExecutor(new CommandHandler());
		this.getCommand("spawn").setExecutor(new CommandHandler());
		this.getCommand("setspawn").setExecutor(new CommandHandler());
		this.getCommand("gmc").setExecutor(new CommandHandler());
		this.getCommand("gms").setExecutor(new CommandHandler());
		this.getCommand("gma").setExecutor(new CommandHandler());
		this.getCommand("tpa").setExecutor(new CommandHandler());
		this.getCommand("tpo").setExecutor(new CommandHandler());
		this.getCommand("hat").setExecutor(new CommandHandler());
	}

	@Override
	public void onDisable() {}
}