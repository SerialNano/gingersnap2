/*
 * CommandHandler.java
 * v1.2.8
 * Last updated 04/15/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import com.serialnano.GingerSnap.handlers.*;

public class CommandHandler implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		CommandFactory factory = new CommandFactory(sender, cmd, label, args); //Instantiate a factory class to use
		CommandHandlerAbstract handler = factory.makeCommand();				   //Get the command from factory
		
		if(handler == null){
			return false;
		}
		
		if(handler.hasPermission()){
			return handler.runCommand();
		} else {
			return handler.noPerm();
		}
	}

}