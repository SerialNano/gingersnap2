/*
 * ChatHandler.java
 * v1.2.9
 * Last updated 05/02/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ChatHandler {
	
	//The instance that executed the command
	protected CommandSender sender;
	
	//The actual command that was executed
	protected Command cmd;
	protected String label;
	
	//Any arguments entered with the command
	protected String[] args;
	
	private String standard(String text){
		return(ChatColor.GOLD + text);
	}
	private String parameter(String text){
		return(ChatColor.DARK_GREEN + text);
	}
	private String emphasis(String text){
		return(ChatColor.AQUA + "" + ChatColor.BOLD + text);
	}
	private String errorFormat(String text){
		return(ChatColor.DARK_RED + text);
	}
	private String headerformat(String text){
		return(ChatColor.DARK_BLUE + "" + ChatColor.BOLD+ text);
	}

	private ChatColor reset = ChatColor.RESET;
	
	//Assign values to the parameters
	public ChatHandler(CommandSender sender, Command cmd, String label, String[] args){
		this.sender = sender;
		this.cmd = cmd;
		this.label = label;
		this.args = args;
	}

	public String allGold(String message){
		return (standard(message) + reset);
	}
	
	public String goldGreen(String message, String player){
		return(standard(message) + " " + parameter(player) + 
				standard(".") + reset);
	}
	
	public String goldGreenGold(String messageA, String player, String messageB){
		return(standard(messageA) + " " + parameter(player) + " " + standard(messageB) + reset);
	}
	
	public String goldGreenGoldGreen(String messageA, String playerA, String messageB, String playerB){
		return(standard(messageA) + " " + parameter(playerA) + " " + standard(messageB) + " " + parameter(playerB) + reset);
	}
	
	public String greenGold(String personA, String message){
		return (parameter(personA) + " " + standard(message) + reset);
	}
	
	public String greenGoldGreen(String personA, String message, String personB){
		return (parameter(personA) + standard(" " + message) + " "
				+ parameter(personB) + standard(".") + reset);
	}
	
	public String greenGoldGreenGold(String personA, String messageA, String personB, String messageB){
		return (parameter(personA) + " " + standard(messageA) + " " + 
				parameter(personB) + " " + standard(messageB) + reset);
	}
	
	@SuppressWarnings("deprecation")
	public String notOnline(){
		return (standard(Bukkit.getServer().getPlayer(this.args[0]).getDisplayName()) + error(" is not online!") + reset);
	}
	
	public String header(String title){
		return (headerformat(title) + reset);
	}
	
	public String announce(String message){
		return (emphasis(message) + reset);
	}
	
	public String error(String message){
		return (errorFormat(message) + reset);
	}
	
	public String blazeAlert() {
		return (ChatColor.RED + "Y" + ChatColor.YELLOW + "o" + ChatColor.DARK_RED + "u " + ChatColor.RED + "w" + 
			ChatColor.YELLOW + "e" + ChatColor.DARK_RED + "r" + ChatColor.RED + "e " + ChatColor.YELLOW + 
			"s" + ChatColor.DARK_RED + "e" + ChatColor.RED + "t " + ChatColor.YELLOW + "a" + ChatColor.DARK_RED 
			+ "b" + ChatColor.RED + "l" + ChatColor.YELLOW + "a" + ChatColor.DARK_RED + "z" + 
			ChatColor.RED + "e" + ChatColor.YELLOW + "!" + ChatColor.RESET);
	}
}