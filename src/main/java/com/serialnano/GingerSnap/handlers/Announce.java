/*
 * Announce.java
 * v1.2.9
 * Last updated 05/01/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Announce extends CommandHandlerAbstract{
	
	public Announce(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		if (this.args.length < 1){
			this.sender.sendMessage(this.chat.error("Please specify the message to broadcast!"));
		} else if (this.args.length > 0){
			String message = args[0];
			boolean first = true;
			for(int i=1; i < args.length; i++){
				message = ((message + args[i]) + " ");
				if(first == true){
					message = (message + " ");
					first = false;
				}
			}
			Bukkit.getServer().broadcastMessage(this.chat.announce(message));
			return true;
		} else {
			return false;
		}
		return false;
	}

	@Override
	public boolean hasPermission() {
		return this.getPerm("snap.announce");
	}

	@Override
	public String associatedCommand() {
		return "announce";
	}

}
