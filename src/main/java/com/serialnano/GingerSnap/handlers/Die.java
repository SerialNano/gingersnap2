/*
 * Die.java
 * v1.2.8
 * Last updated 04/14/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Die extends CommandHandlerAbstract{
	public Die(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		if (!(this.sender instanceof Player)) {
			this.sender.sendMessage(this.chat.error("The Console cannot kill itself!"));
			return true;
		} else if (this.args.length != 0) {
			return false;
		} else {
			this.sender.sendMessage(this.chat.greenGoldGreenGold("You", "bring your pickaxe upon",
					"your own", "chest."));
			
			((Player) this.sender).setHealth(0.0);
			Bukkit.getServer().broadcastMessage(this.chat.greenGold(this.sender.getName(), 
					"brought a pickaxe upon their own chest!"));
			
			return true;
		}
	}

	@Override
	public boolean hasPermission() {
		return this.getPerm("snap.die");
	}

	@Override
	public String associatedCommand() {
		return "die";
	}
}