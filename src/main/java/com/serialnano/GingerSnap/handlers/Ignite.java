/*
 * Ignite.java
 * v1.2.9
 * Last updated 05/01/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Ignite extends CommandHandlerAbstract{
	public Ignite(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean runCommand() {
		if (this.args.length < 1) {
			return false;
		} else if (this.args.length == 1) {
			if (!(this.sender instanceof Player)) {
				this.sender.sendMessage(this.chat.error("You must specify a player AND a time when "
						+ "igniting via Console!"));
				return true;
			} else {
				try {
					((Player) this.sender).setFireTicks(((Integer.parseInt(this.args[0])) * 20));
					return true;
				} catch (NumberFormatException nfe) {
					return false;
				}
			}
		} else if (this.args.length == 1) {
			Player target = (Player) sender;
			target.setFireTicks(((Integer.parseInt(args[0])) * 20));
			this.sender.sendMessage(this.chat.blazeAlert());
			return true;
		} else if (this.args.length == 2) {
			Player target = Bukkit.getServer().getPlayer(this.args[0]);
			if (target == null) {
				this.sender.sendMessage(this.chat.notOnline());
				return true;
			} else {
				try {
					this.sender.sendMessage(this.chat.greenGoldGreenGold("You", "have set",
							Bukkit.getServer().getPlayer(this.args[0]).getDisplayName(), "on fire!"));
					target.setFireTicks(((Integer.parseInt(args[1])) * 20));
					target.sendMessage(this.chat.blazeAlert());
					return true;
				} catch (NumberFormatException nfe) {
					return false;
				}
			}
		} else {
			return false;
		}
	}
	
	@Override
	public boolean hasPermission() {
		if (this.args.length != 0){
			return this.getPerm("snap.ignore.others");
		} else {
			return(this.getPerm("snap.ignore"));
		}
	}

	@Override
	public String associatedCommand() {
		return "ignite";
	}
	
}