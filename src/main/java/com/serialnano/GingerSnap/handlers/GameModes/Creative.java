/*
 * Creative.java
 * v1.2.9
 * Last updated on 04/16/2014
 * All Rights REserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers.GameModes;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.serialnano.GingerSnap.handlers.CommandHandlerAbstract;

public class Creative extends CommandHandlerAbstract{

	public Creative(CommandSender sender, Command cmd, String label,
			String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		if (this.args.length < 1) {
			if (!(this.sender instanceof Player)) {
				this.sender.sendMessage(this.chat.error("You must specify a "
						+ "player when setting game mode via Console!"));
				return true;
			} else {
				Player target = (Player) sender;
				target.setGameMode(GameMode.CREATIVE);
				target.sendMessage(this.chat.greenGoldGreen("You", "set your game mode to", "Creative"));
				return true;
			}
		} else if (this.args.length == 1) {
			@SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(this.args[0]);
			if (target == null) {
				this.sender.sendMessage(this.chat.notOnline());
				return true;
			} else {
				target.setGameMode(GameMode.CREATIVE);
				String senderName = ((Player) sender).getDisplayName();
				target.sendMessage(this.chat.greenGoldGreen(senderName, "set your game mode to", "Creative"));
				
				String targetName = (target.getDisplayName() + "'s");
				sender.sendMessage(this.chat.greenGoldGreen(targetName, "game mode set to", "Creative"));
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean hasPermission() {
		if (this.args.length != 0){
			return this.getPerm("snap.gm.creative.others");
		} else {
			return(this.getPerm("snap.gm.creative"));
		}
	}

	@Override
	public String associatedCommand() {
		return "gmc";
	}

}