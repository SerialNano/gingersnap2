/*
 * Hat.java
 * v1.2.9
 * Last updated 04/22/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Hat extends CommandHandlerAbstract{

	public Hat(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		if(!(this.sender instanceof Player)){
			this.sender.sendMessage(this.chat.error("Only players can place things on their head!"));
			return true;
		} else {
			Player player = (Player)this.sender;
			ItemStack playerItem = new ItemStack(player.getItemInHand());
			
			player.getInventory().getHelmet().setType(playerItem.getType());
			player.getInventory().getItemInHand().setAmount(0);
			
			player.sendMessage(this.chat.allGold("Enjoy your fancy new hat!"));
			
			return true;
		}
	}

	@Override
	public boolean hasPermission() {
		return this.getPerm("snap.hat");
	}

	@Override
	public String associatedCommand() {
		return "hat";
	}

}
