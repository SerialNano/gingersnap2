/*
 * Spawn.java
 * v1.2.9.2
 * Last updated 05/02/2014
 * All Rights Revserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers.Teleport;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.serialnano.GingerSnap.GingerSnap;
import com.serialnano.GingerSnap.handlers.CommandHandlerAbstract;

public class Spawn extends CommandHandlerAbstract{

	public Spawn(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean runCommand() {
		Plugin plugin = new GingerSnap();
		String worldName = plugin.getConfig().getString("spawn.world");
		
		Location spawnpoint = new Location(null, 0, 0, 0, 0, 0);
		spawnpoint.setX(plugin.getConfig().getDouble("spawn.x"));
		spawnpoint.setY(plugin.getConfig().getDouble("spawn.y"));
		spawnpoint.setZ(plugin.getConfig().getDouble("spawn.z"));
		spawnpoint.setWorld(Bukkit.getServer().getWorld(worldName));
		spawnpoint.setDirection(plugin.getConfig().getVector("spawn.direction"));
		spawnpoint.setPitch((float) plugin.getConfig().getDouble("spawn.pitch"));
		spawnpoint.setYaw((float) plugin.getConfig().getDouble("spawn.yaw"));
		
		if (!(this.sender instanceof Player)){
			this.sender.sendMessage(this.chat.error("Only players can teleport to spawn!"));
			return true;
		} else if (spawnpoint.getX() == 0 && spawnpoint.getY() == 0 && spawnpoint.getZ() == 0){
			this.sender.sendMessage(this.chat.error("The global spawnpoint has not been set! Use /setspawn!"));
			return true;
		} else if(this.args.length != 0){
			this.sender.sendMessage(this.chat.goldGreenGold("Teleporting", Bukkit.getServer().getPlayer(this.args[0]).getDisplayName(), "to spawn."));
			
			Player target = Bukkit.getServer().getPlayer(this.args[0]);
			
			target.sendMessage(this.chat.greenGold("You", "were teleported to spawn."));
			teleportToSpawn(target, spawnpoint);
			
			return true;
		} else {
			Player target = ((Player)sender);
			
			target.sendMessage(this.chat.allGold("Teleporting you to spawn..."));
			teleportToSpawn(target, spawnpoint);
			
			return true;
		}
	}
	
	public void teleportToSpawn(Player target, Location spawnpoint){
		target.getLocation().setWorld(spawnpoint.getWorld());
		target.getLocation().setX(spawnpoint.getX());
		target.getLocation().setY(spawnpoint.getY());
		target.getLocation().setZ(spawnpoint.getZ());
		target.getLocation().setDirection(spawnpoint.getDirection());
		target.getLocation().setPitch(spawnpoint.getPitch());
		target.getLocation().setYaw(spawnpoint.getYaw());
	}

	@Override
	public boolean hasPermission() {
		if (this.args.length != 0){
			return this.getPerm("snap.spawn.others");
		} else {
			return(this.getPerm("snap.spawn"));
		}
	}

	@Override
	public String associatedCommand() {
		return "spawn";
	}
	
}
