/*
 * SetSpawn.java
 * v1.2.9.2
 * Last updated on 05/02/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers.Teleport;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.serialnano.GingerSnap.GingerSnap;
import com.serialnano.GingerSnap.handlers.CommandHandlerAbstract;

public class SetSpawn extends CommandHandlerAbstract{

	public SetSpawn(CommandSender sender, Command cmd, String label,
			String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		if (!(this.sender instanceof Player)){
			this.sender.sendMessage(this.chat.error("You must be a player to set the spawnpoint!"));
			return true;
		} else {
			Player target = (Player) sender;
			Plugin plugin = new GingerSnap();
			Location spawnpoint = new Location(null, 0, 0, 0, 0, 0);
			
			spawnpoint.setX(target.getLocation().getX());
			spawnpoint.setY(target.getLocation().getY());
			spawnpoint.setZ(target.getLocation().getZ());
			spawnpoint.setWorld(target.getLocation().getWorld());
			spawnpoint.setDirection(target.getLocation().getDirection());
			spawnpoint.setPitch(target.getLocation().getPitch());
			spawnpoint.setYaw(target.getLocation().getYaw());
			
			double spawnPitch = (double) spawnpoint.getPitch();
			double spawnYaw = (double) spawnpoint.getYaw();
			
			plugin.getConfig().set("spawn.x", spawnpoint.getX());
			plugin.getConfig().set("spawn.y", spawnpoint.getY());
			plugin.getConfig().set("spawn.z", spawnpoint.getZ());
			plugin.getConfig().set("spawn.world", spawnpoint.getWorld());
			plugin.getConfig().set("spawn.direction", spawnpoint.getDirection());
			plugin.getConfig().set("spawn.pitch", spawnPitch);
			plugin.getConfig().set("spawn.yaw", spawnYaw);
			
			String locationCoords = (spawnpoint.getX() + ", " + spawnpoint.getY() + ", " + spawnpoint.getZ());
			this.sender.sendMessage(this.chat.goldGreenGoldGreen("Set the spawnpoint to", locationCoords, "in world", spawnpoint.getWorld().getName()));
			
			return true;
		}
	}

	@Override
	public boolean hasPermission() {
		return this.getPerm("snap.setspawn");
	}
	
	@Override
	public String associatedCommand() {
		return "setspawn";
	}

}
