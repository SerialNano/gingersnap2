/*
 * Tpo.java
 * v1.2.9
 * Last updated on 04/17/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers.Teleport;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.serialnano.GingerSnap.handlers.CommandHandlerAbstract;

public class Tpo extends CommandHandlerAbstract{

	public Tpo(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean runCommand() {
		if(!(this.sender instanceof Player)){
			this.sender.sendMessage(this.chat.error("You must be a player to teleport to another player!"));
			return true;
		} else if (this.args.length != 1) {
			this.sender.sendMessage(this.chat.error("Use like this, for example:  /tpo Notch"));
			return true;
		} else {
			Player target = Bukkit.getServer().getPlayer(args[0]);
			if(target == null){
				this.sender.sendMessage(this.chat.notOnline());
				return true;
			} else {
				TeleportObject teleport = new TeleportObject();
				teleport.id = 0;
				teleport.sender = (Player) this.sender;
				teleport.receiver = target;
				teleport.accepted = true;
				
				this.sender.sendMessage(this.chat.goldGreenGoldGreen("Teleporting", "you", "to",target.getDisplayName()));
				
				((Player) this.sender).getLocation().setDirection(target.getLocation().getDirection());
				((Player) this.sender).getLocation().setPitch(target.getLocation().getPitch());
				((Player) this.sender).getLocation().setYaw(target.getLocation().getYaw());
				((Player) this.sender).getLocation().setWorld(target.getLocation().getWorld());
				((Player) this.sender).getLocation().setX(target.getLocation().getX());
				((Player) this.sender).getLocation().setY(target.getLocation().getY());
				((Player) this.sender).getLocation().setZ(target.getLocation().getZ());
				
				return true;
			}
		}
	}

	@Override
	public boolean hasPermission() {
		return (this.getPerm("snap.tpo"));
	}

	@Override
	public String associatedCommand() {
		return "tpo";
	}

	
}
