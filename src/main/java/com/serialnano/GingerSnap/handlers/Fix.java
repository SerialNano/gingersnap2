/*
 * Fix.java
 * v1.2.8
 * Last updated 04/14/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Fix extends CommandHandlerAbstract{
	
	public Fix(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		Player player = (Player)this.sender;
		ItemStack airStack = new ItemStack(Material.AIR);
		ItemStack playerItem = new ItemStack(player.getItemInHand());
		if (!(this.sender instanceof Player)) {
			this.sender.sendMessage(this.chat.error("The Console has no inventory to repair!"));
			return true;
		} else if (this.args.length != 0) {
			return false;
		} else if (playerItem == airStack || playerItem == null){
			this.sender.sendMessage(this.chat.error("You cannot fix air--you can't even break it!"));
			return true;
		} else {
			player.getItemInHand().setDurability((short) 1.0);
			this.sender.sendMessage(this.chat.allGold("Repaired item successfully!"));
			return true;
		}
	}
	
	@Override
	public boolean hasPermission() {
		return this.getPerm("snap.fix");
	}

	@Override
	public String associatedCommand() {
		return "fix";
	}

}