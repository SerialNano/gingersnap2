/*
 * Soar.java
 * v1.2.9
 * Last updated 04/16/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Soar extends CommandHandlerAbstract{
	public Soar(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean runCommand() {
		if (this.args.length < 1) {
			if (!(this.sender instanceof Player)) {
				this.sender.sendMessage(this.chat.error("The Console is not capable of flight!"));
				return true;
			} else {
				if (((Player)this.sender).getAllowFlight()){
					((Player)this.sender).setAllowFlight(false);
					this.sender.sendMessage(this.chat.greenGoldGreen("You", "disabled flight for", "yourself"));
					} else {
					((Player)this.sender).setAllowFlight(true);
					this.sender.sendMessage(this.chat.greenGoldGreen("You", "enabled flight for", "yourself"));
					}
				return true;
			}
		} else if (this.args.length == 1) {
			Player target = Bukkit.getServer().getPlayer(this.args[0]);
			if (target == null) {
				this.sender.sendMessage(this.chat.notOnline());
				return true;
			} else {
				if (target.getAllowFlight()){
					target.setAllowFlight(false);
					this.sender.sendMessage(this.chat.greenGoldGreen("You", "disabled flight for", Bukkit.getServer().getPlayer(this.args[0]).getDisplayName()));
					target.sendMessage(this.chat.greenGold("Your", "fly mode was disabled."));
				} else {
					target.setAllowFlight(true);
					this.sender.sendMessage(this.chat.greenGoldGreen("You", "enabled flight for", Bukkit.getServer().getPlayer(this.args[0]).getDisplayName()));
					target.sendMessage(this.chat.greenGold("Your", "fly mode was enabled."));
				}
				return true;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public boolean hasPermission() {
		if (this.args.length != 0){
			return this.getPerm("snap.togglesoar.others");
		} else {
			return(this.getPerm("snap.togglesoar"));
		}
	}

	@Override
	public String associatedCommand() {
		return "soar";
	}
	
}