/*
 * GingerSnapInfo.java
 * v1.3.0
 * Last updated 05/02/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;


public class GingerSnapInfo extends CommandHandlerAbstract{
	public GingerSnapInfo(CommandSender sender, Command cmd, String label,
			String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {

		this.sender.sendMessage(this.chat.header("GingerSnap Plugin Information"));
		this.sender.sendMessage(this.chat.greenGold("Version:", "1.3.0"));
		this.sender.sendMessage(this.chat.greenGold("Developer:", "J. Thibeault (aka stormhunted, joshuat3500, etc)"));
		this.sender.sendMessage(this.chat.greenGold("Bugs:", "Report glitches, bugs, and gateways out of the Matrix"));
		this.sender.sendMessage(this.chat.allGold("via our ticket tracker on our Bukkit page."));
		return true;
	}
	
	@Override
	public boolean hasPermission() {
		return this.getPerm("snap.snap");
	}

	@Override
	public String associatedCommand() {
		return "gingersnap";
	}
	
}