/*
 * Bench.java
 * v1.2.9
 * Last updated 04/16/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Bench extends CommandHandlerAbstract{

	public Bench(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean runCommand() {
		if (this.args.length < 1) {
			if (!(this.sender instanceof Player)) {
				this.sender.sendMessage(this.chat.error("You must provide the name of player to open a bench for!"));
				return true;
			} else {
				((Player)this.sender).openWorkbench(null, false);
				this.sender.sendMessage(this.chat.greenGoldGreen("You", "opened a workbench for", "yourself"));
				return true;
			}
		} else if (this.args.length == 1) {
			Player target = Bukkit.getServer().getPlayer(this.args[0]);
			if (target == null) {
				this.sender.sendMessage(this.chat.notOnline());
				return true;
			} else {
				target.openWorkbench(null, false);
				this.sender.sendMessage(this.chat.greenGoldGreen("You", "opened a workbench for", Bukkit.getServer().getPlayer(this.args[0]).getDisplayName()));
				target.openWorkbench(null, false);
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean hasPermission() {
		if (this.args.length != 0){
			return this.getPerm("snap.bench.others");
		} else {
			return(this.getPerm("snap.bench"));
		}
	}

	@Override
	public String associatedCommand() {
		return "bench";
	}

}