/*
 * Boom.java
 * v1.2.9
 * Last updated 04/16/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;

public class Boom extends CommandHandlerAbstract{

	public Boom(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean runCommand() {
		if (this.args.length < 1){
			if(!(this.sender instanceof Player)) {
				this.sender.sendMessage(this.chat.error("You must specify a player to invoke /boom on!"));
				return true;
			} else {
				this.sender.sendMessage(this.chat.announce("Brace for impact!"));
				Player target = ((Player)this.sender);
				Location playerLocation = target.getLocation();
				Entity tnt = target.getWorld().spawnEntity(playerLocation, EntityType.PRIMED_TNT);
				((TNTPrimed)tnt).setFuseTicks(200);	
				return true;
			}
		} else if (this.args.length == 1){
			Player target = Bukkit.getServer().getPlayer(args[0]);
			if (target == null) {
				this.sender.sendMessage(this.chat.notOnline());
				return true;
			} else {
				this.sender.sendMessage(this.chat.goldGreen("Dropping a bomb on", Bukkit.getServer().getPlayer(this.args[0]).getDisplayName()));
				target.sendMessage(this.chat.announce("Brace for impact!"));
				Location playerLocation = target.getLocation();
				Entity tnt = target.getWorld().spawnEntity(playerLocation, EntityType.PRIMED_TNT);
				((TNTPrimed)tnt).setFuseTicks(200);
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean hasPermission() {
		if (this.args.length != 0){
			return this.getPerm("snap.boom.others");
		} else {
			return(this.getPerm("snap.boom"));
		}
	}

	@Override
	public String associatedCommand() {
		return "boom";
	}
}
