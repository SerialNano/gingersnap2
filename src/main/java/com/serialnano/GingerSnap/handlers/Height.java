/*
 * Height.java
 * v1.2.9
 * Last updated 04/16/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Height extends CommandHandlerAbstract{
	public Height(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		if (this.args.length < 1) {
			if (!(this.sender instanceof Player)) {
				this.sender.sendMessage(this.chat.error("You must specify a "
						+ "player when using /height via Console!"));
				return true;
			} else {
				int playerHeight = (int) ((Player)this.sender).getLocation().getY();
				String playerHeightStr = (playerHeight + " blocks");
				this.sender.sendMessage(this.chat.greenGoldGreenGold("Your", "current height is",
						playerHeightStr, "."));
				return true;
			}
		} else if (this.args.length == 1) {
			@SuppressWarnings("deprecation")
			Player target = Bukkit.getServer().getPlayer(this.args[0]);
			if (target == null) {
				this.sender.sendMessage(this.chat.notOnline());
				return true;
			} else {
				@SuppressWarnings("deprecation")
				String targetName = (Bukkit.getServer().getPlayer(this.args[0]).getDisplayName() + "'s");
				int targetHeight = (int) (target.getLocation().getY());
				String targetHeightStr = (targetHeight + "blocks");
				this.sender.sendMessage(this.chat.greenGoldGreenGold(targetName, "current height is",
						targetHeightStr, "."));
				return true;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public boolean hasPermission() {
		if (this.args.length != 0){
			return this.getPerm("snap.height.others");
		} else {
			return(this.getPerm("snap.height"));
		}
	}

	@Override
	public String associatedCommand() {
		return "height";
	}
	
}