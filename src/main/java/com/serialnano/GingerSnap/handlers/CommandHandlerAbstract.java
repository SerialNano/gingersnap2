/*
 * CommandHandlerAbstract.java
 * v1.2.8
 * Last updated 04/14/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.serialnano.GingerSnap.ChatHandler;

public abstract class CommandHandlerAbstract {
	public CommandSender sender;
	public Command cmd;
	public String label;
	public String[] args;
	public ChatHandler chat;
	
	public CommandHandlerAbstract(CommandSender sender, Command cmd, String label, String[] args){
		this.sender = sender;
		this.cmd = cmd;
		this.label = label;
		this.args = args;
		this.chat = new ChatHandler(sender, cmd, label, args);
	}
	
	public abstract boolean runCommand();
	public abstract boolean hasPermission();
	public abstract String associatedCommand();
	
	public boolean noPerm(){
		sender.sendMessage(this.chat.error("You don't have permission to use that command!"));
		return true;
	}
	
	public boolean getPerm(String permNode){
		return ((Player)this.sender).hasPermission(permNode);
	}
}