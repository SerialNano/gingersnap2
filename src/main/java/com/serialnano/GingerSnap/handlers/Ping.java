/*
 * Ping.java
 * v1.2.8
 * Last updated 04/14/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap.handlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Ping extends CommandHandlerAbstract{
	public Ping(CommandSender sender, Command cmd, String label, String[] args) {
		super(sender, cmd, label, args);
	}

	@Override
	public boolean runCommand() {
		if (this.args.length > 0) {
			return false;
		} else {
			this.sender.sendMessage(this.chat.goldGreen("Recieved reply from", "127.0.0.1"));
			return true;
		}
	}
	
	@Override
	public boolean hasPermission() {
		return this.getPerm("snap.ping");
	}

	@Override
	public String associatedCommand() {
		return "ping";
	}
	
}