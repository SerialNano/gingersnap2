/*
 * CommandFactory.java
 * v1.2.9
 * Last updated 05/02/2014
 * All Rights Reserved, J. Thibeault
 */

package com.serialnano.GingerSnap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.serialnano.GingerSnap.handlers.*;
import com.serialnano.GingerSnap.handlers.GameModes.*;
import com.serialnano.GingerSnap.handlers.Teleport.*;

public class CommandFactory {
	//The instance executing the command
	protected CommandSender sender;
	
	//The actual command that was executed
	protected Command cmd;
	protected String label;	
	
	//Any arguments used after the command
	protected String[] args;
	
	//Assign values to needed objects
	public CommandFactory(CommandSender sender, Command cmd, String label, String[] args){
		this.sender = sender;
		this.cmd = cmd;
		this.label = label;
		this.args = args;
	}
	
	//Return command class for the command entered
	public CommandHandlerAbstract makeCommand(){
		if(cmd.getName().equalsIgnoreCase("announce")){
			return new Announce(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("bench")){
			return new Bench(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("boom")){
			return new Boom(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("die")){
			return new Die(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("fix")){
			return new Fix(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("gingersnap")){
			return new GingerSnapInfo(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("height")){
			return new Height(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("ignite")){
			return new Ignite(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("ping")){
			return new Ping(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("soar")){
			return new Soar(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("gma")){
			return new Adventure(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("gms")){
			return new Survival(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("gmc")){
			return new Creative(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("setspawn")){
			return new SetSpawn(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("spawn")){
			return new Spawn(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("tpo")){
			return new Tpo(sender, cmd, label, args);
		} else if (cmd.getName().equalsIgnoreCase("hat")){
			return new Hat(sender, cmd, label, args);
		} else {
			return null;
		}
	}
}